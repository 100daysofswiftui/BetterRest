//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Pascal Hintze on 24.10.2023.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
