# BetterRest

BetterRest is an app that will help coffee drinkers get a good night’s sleep by giving them information about when they should go to bed based on their coffe consumption.


This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/betterrest-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Stepper
- DatePicker
- DateFormatter
- Create ML
- Core ML